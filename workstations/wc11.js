var http=require('http');

var WorkCell=require('../classes/WSclass.js');

var name='11';
var capability='green';
var port='200'+name;
var connections=['http://127.0.0.1:200'+(parseInt(name)+1)];
var url='http://127.0.0.1:200'+name;

var wc=new WorkCell(name, capability, port, url);

wc.addConnections(connections);
wc.runServer();
wc.initialize();