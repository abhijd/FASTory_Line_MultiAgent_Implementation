const request = require('request');
var http = require('http');

"use strict";
module.exports= class{
    constructor(name, capability, port, url){
        this.name=name;
        this.capability=capability;
        this.connections=[];
        this.port=port;
        this.url=url;
        this.order={};
        this.palletId='';
        this.event={};
    }

    addConnections(arrayOfConnections){
        var ref = this;
        if(Array.isArray(arrayOfConnections)){
            arrayOfConnections.forEach(function(x){
                ref.connections.push(x);
            });
        }else{
            console.log("Connections not added, input must be in the form of array.");
        }
    }

    runServer(){

        var ref = this;

        var myServer = http.createServer(function(req, res) {
            var method = req.method;
            console.log("Method: " + method);

            if(method == 'GET'){
                //Handle GET method.
                res.statusCode = 200;
                res.setHeader('Content-Type', 'text/plain');
                res.end('Agent ' + ref.name + ' is running.');
            } else if(method == 'POST'){
                //Handle POST method.
                var body = []; //Getting data: https://nodejs.org/en/docs/guides/anatomy-of-an-http-transaction/
                req.on('data', function(chunk) {
                    body.push(chunk);
                    console.log("Body???: " + body.toString());
                    var reqData=JSON.parse(body);
                    switch(reqData.messageType){
                        case "order":
                            ref.order=reqData.order;
                            ref.service('LoadPallet');
                            var controller=0;
                            var myInterval=setInterval(function () {
                                if(ref.event.id=="PalletLoaded"){
                                    ref.service('TransZone35');
                                }
                                if(ref.event.id=="Z5_Changed"){
                                    var findd=ref.formFindMessage('paper', [], ref.event.PalletID, ref.url )
                                    for(var i=0; i < ref.connections.length; i++){
                                            findd.message.path.push(ref.url);
                                            ref.makeRequest(ref.connections[i], JSON.stringify(findd));
                                    }
                                    clearInterval(myInterval);
                                }
                                controller++;
                                if(controller>20000){
                                    clearInterval(myInterval);
                                }

                            },5)
                        break;
                        case "find":
                            if(reqData.message.capability == ref.capability){
                                ref.palletId=reqData.message.palletId;
                                reqData.message.path.push(ref.url);
                                console.log("FOUND PATH");
                                console.log(reqData.message.path);
                                var foundPath=ref.formFoundPathMessage(reqData.message.path, reqData.message.capability, reqData.message.palletID);
                                ref.makeRequest(reqData.message.whoAsked, JSON.stringify(foundPath));

                            }
                            else{
                                for(var i=0; i < ref.connections.length; i++){
                                    if(reqData.message.path.indexOf(ref.connections[i])==-1){
                                        // TODO send this same request to the connections
                                        reqData.message.path.push(ref.url);
                                        ref.makeRequest(ref.connections[i], JSON.stringify(reqData));
                                    }
                                }
                            }
                            break;
                        case "pass":
                            if(reqData.message.path.length==1 && reqData.message.path[0]==ref.url){
                                /**
                                 * TODO Execute order drawing, after completion add done status 1 to that particular draw,
                                 * look for the next draw status.
                                 * If All draw status is done, start building path for unloading station
                                 * if a draw status is not done, initiate path building with that capability
                                 */
                                console.log("Reached Execution Station");
                                ref.order=reqData.message.order;
                                ref.service('TransZone12');
                                var controller=0; var controller2=false; var controller3=false;
                                var myInterval=setInterval(function () {
                                    if(ref.event.id=="Z2_Changed" && ref.event.payload.PalletID!=-1){
                                        if(!controller2) {
                                            ref.service('TransZone23');
                                            controller2 == true;
                                        }
                                    }
                                    if(ref.event.id=="Z3_Changed" && ref.event.payload.PalletID!=-1){
                                        ref.service('UnloadPallet');
                                        clearInterval(myInterval);
                                    }
                                    controller++;
                                    if(controller>20000){
                                        clearInterval(myInterval);
                                    }
                                },20)
                            }else if(reqData.message.path.length>1 && reqData.message.path[0]==ref.url){
                                reqData.message.path.shift();
                                ref.service('TransZone12');
                                var controller=0; var controller2=false; var controller3=false;
                                var myInterval=setInterval(function () {
                                    if(ref.event.id=="Z2_Changed" && ref.event.payload.PalletID!=-1){
                                        if(!controller2) {
                                            ref.service('TransZone23');
                                            controller2 == true;
                                        }
                                    }
                                    if(ref.event.id=="Z3_Changed" && ref.event.payload.PalletID!=-1){
                                        if(!controller3) {
                                            ref.service('TransZone35');
                                            controller2 == true;
                                        }
                                    }
                                    if(ref.event.id=="Z5_Changed" && ref.event.payload.PalletID!=-1){
                                        ref.makeRequest(reqData.message.path[0], JSON.stringify(reqData));
                                        clearInterval(myInterval);
                                    }
                                    controller++;
                                    if(controller>20000){
                                        clearInterval(myInterval);
                                    }
                                },20)
                            }
                            break;
                        case "found_path":
                            // TODO pass pallet to the start of the next station and start a pass request
                            var passs=ref.formPassMessage(ref.order, reqData.message.path, reqData.message.palletId);
                            ref.makeRequest(reqData.message.path[0], JSON.stringify(passs));
                            break;
                        default:
                            ref.event=reqData;
                            break;
                    }
                    res.end("OK"); //Avoid sender waiting for a reply.
                });

            }
        });

        myServer.listen(this.port, "127.0.0.1", () => {
            console.log('Agent server ' + ref.name + ' is running at http://127.0.0.1:' + this.port);
    });
    };


    makeRequest(whoToAsk, req) {

        var options = {

            method: 'post',
            body: req, // Javascript object
            url:  whoToAsk,
            headers: {
                'Content-Type': 'application/json'
            }
        };

        //Print the result of the HTTP POST request
        request(options, function (err, res, body) {
            if (err) {
                console.log('Error :', err);
                return;
            }
            console.log(body);
        });

    }

    formFoundPathMessage(pathArray, capability, palletId){
        var foundPathMessage={
            messageType: 'found_path',
            message:{
                path:pathArray,
                capability: capability,
                palletId: palletId
            }

        }
        // return JSON.stringify(foundPathMessage);
        return foundPathMessage;
    }
    formFindMessage(cap,  path,  palletId,  whoAsked  ){
        var findMessage={
            messageType:  'find',
            message:{
                capability: cap,
                path:  path,
                palletId:  palletId,
                whoAsked:  whoAsked
            }
        }
        // return JSON.stringify(findMessage);
        return findMessage;

    }

    formPassMessage(order,  path,  palletId){
        var passMessage={
            messageType: 'pass',
            message:{
                order: order,
                path: path,
                palletId:  palletId
            }
        }
        // return JSON.stringify(passMessage);
        return passMessage;
    }

    draw( drawseq){
        var reqURL="http://127.0.0.1:3000/RTU/SimROB"+this.name+"/services/Draw"+drawseq;
        var bod={destUrl  :  "http://hostname"};
        this.makeRequest(reqURL, JSON.stringify(bod));
    }
    subscribeEvents(){
        var events=[
            "http://127.0.0.1:3000/RTU/SimROB"+this.name+"/events/PalletLoaded/notifs",
            "http://127.0.0.1:3000/RTU/SimROB"+this.name+"/events/PalletUnloaded/notifs",
            "http://127.0.0.1:3000/RTU/SimCNV"+this.name+"/events/Z1_Changed/notifs",
            "http://127.0.0.1:3000/RTU/SimCNV"+this.name+"/events/Z2_Changed/notifs",
            "http://127.0.0.1:3000/RTU/SimCNV"+this.name+"/events/Z3_Changed/notifs", ,
            "http://127.0.0.1:3000/RTU/SimCNV"+this.name+"/events/Z5_Changed/notifs",
        ];
        var bod={destUrl :  this.url};

        var ref=this;
        var count=0; var stopCount=events.length;
        var myInterval = setInterval(function(){
            ref.makeRequest(events[count], JSON.stringify(bod));
            count++;
            if(count==stopCount){
                clearInterval(myInterval);
            }
        }, 200);

    }

    service(serviceName){
        var RTU='ROB';
        if(serviceName.indexOf("Z")!=-1){
            RTU='CNV'
        }
        var service="	http://127.0.0.1:3000/RTU/Sim"+RTU+this.name+"/services/"+serviceName;
        var bod={destUrl :  this.url};
        this.makeRequest(service, JSON.stringify(bod));

    }


}


// function REST_Class(ServiceOrEventName, ServicesOrEvents , RTUID, desURL ){
//
//     if(ServicesOrEvents=="services"){
//         this.urll= "http://192.168."+WorkcellNumber+"."+RTUID+"/rest/"+ServicesOrEvents+"/"+ServiceOrEventName
//     }else if(ServicesOrEvents=="events"){
//         this.urll= "http://192.168."+WorkcellNumber+"."+RTUID+"/rest/"+ServicesOrEvents+"/"+ServiceOrEventName+"/notifs"
//     }
//
//     this.option={
//         method: 'post',
//         body: {"destUrl":desURL},
//         json: true,
//         headers: {
//             'Content-Type': 'application/json'
//         },
//         url: this.urll
//     };
//     this.sendRestRequest=function () {
//         console.log("Req Sent: "+this.urll);
//         console.log(this.option);
//         request(this.option, function (err, res, body) {
//             console.log("After Res"+this.urll);
//
//             if (err) {
//                 console.log('Error :', err);
//                 return;
//             }else {
//                 console.log(' Body :', body);
//                 console.log(' res status code :', res.statusCode);
//                 console.log(' res status message :', res.statusMessage);
//                 // console.log("err: ", err, Object.keys(err));
//                 // console.log( JSON.parse(body.toString()) );
//             }
//         });
//     }
// }

// var MakeAllObjects= function () {
//     Calibrate=new REST_Class("Calibrate", "services",ROB, desURL);
//     Calibrate.option.body={};
//     // Rest Request options for Pallet Transfer from one Zone to another
//     TransZone12=new REST_Class("TransZone12", "services",CNV, desURL);
//     TransZone23=new REST_Class("TransZone23", "services",CNV, desURL);
//     TransZone35=new REST_Class("TransZone35", "services",CNV, desURL);
//     TransZone14=new REST_Class("TransZone14", "services",CNV, desURL);
//     TransZone45=new REST_Class("TransZone45", "services",CNV, desURL);
// // Rest Request options for zone state
//     Z1=new REST_Class("Z1", "services",CNV, "");
//     Z1.body="";
//     Z2=new REST_Class("Z2", "services",CNV, "");
//     Z2.body="";
//     Z3=new REST_Class("Z3", "services",CNV, "");
//     Z3.body="";
//     Z4=new REST_Class("Z4", "services",CNV, "");
//     Z4.body="";
//     Z5=new REST_Class("Z5", "services",CNV, "");
//     Z5.body="";
// // Rest Request options for drawing recipe
//     Draw1=new REST_Class("Draw1", "services",ROB, desURL);
//     Draw2=new REST_Class("Draw2", "services",ROB, desURL);
//     Draw3=new REST_Class("Draw3", "services",ROB, desURL);
//     Draw4=new REST_Class("Draw4", "services",ROB, desURL);
//     Draw5=new REST_Class("Draw5", "services",ROB, desURL);
//     Draw6=new REST_Class("Draw6", "services",ROB, desURL);
//     Draw7=new REST_Class("Draw7", "services",ROB, desURL);
//     Draw8=new REST_Class("Draw8", "services",ROB, desURL);
//     Draw9=new REST_Class("Draw9", "services",ROB, desURL);
// // Rest Request options for changing Pen color
//     ChangePenRED=new REST_Class("ChangePenRED", "services",ROB, desURL);
//     ChangePenGREEN=new REST_Class("ChangePenGREEN", "services",ROB, desURL);
//     ChangePenBLUE=new REST_Class("ChangePenBLUE", "services",ROB, desURL);
// // Rest Request options for querying current pen color
//     GetPenColor=new REST_Class("GetPenColor", "services",ROB, "");
//     GetPenColor.body="";
// // Rest Request options for changing paper in pallet
//     ReplacePaper=new REST_Class("ReplacePaper", "services",ROB, desURL);
//
// // Rest Request options event notification subsciptions
//     Z1_Changed=new REST_Class("Z1_Changed", "events" ,CNV, desURL);
//     Z2_Changed=new REST_Class("Z2_Changed", "events" ,CNV, desURL);
//     Z3_Changed=new REST_Class("Z3_Changed", "events" ,CNV, desURL);
//     Z4_Changed=new REST_Class("Z4_Changed", "events" ,CNV, desURL);
//     Z5_Changed=new REST_Class("Z5_Changed", "events" ,CNV, desURL);
//
//     PenChangeStarted=new REST_Class("PenChangeStarted", "events",ROB, desURL);
//     PenChangeEnded=new REST_Class("PenChangeEnded", "events",ROB, desURL);
//     DrawStartExecution=new REST_Class("DrawStartExecution", "events",ROB, desURL);
//     DrawEndExecution=new REST_Class("DrawEndExecution", "events",ROB, desURL);
//
//     // Subscribe for the event notification
//     // Z1_Changed.sendRestRequest();
//     // Z2_Changed.sendRestRequest();
//     // Z3_Changed.sendRestRequest();
//     // Z4_Changed.sendRestRequest();
//     // Z5_Changed.sendRestRequest();
//     //
//     // PenChangeStarted.sendRestRequest();
//     // PenChangeEnded.sendRestRequest();
//     // DrawStartExecution.sendRestRequest();
//     // DrawEndExecution.sendRestRequest();
//
//
//     // console.log(TransZone12.option);
//
//     // console.log('Z1: '+Z2.option.headers+Z2.option.body.destUrl );
//     // console.log('Z2'+Z2.option.headers+Z2.option.body);
//
//     // GetPenColor.sendRestRequest();
//
// };