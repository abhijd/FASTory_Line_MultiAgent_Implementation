# FASTory Line MultiAgent Implementation


**This is an implementation of the [FASTory](http://www.tut.fi/en/fast/fastory/index.htm) as a multi-agent or distributed system.**
## How to Run the Application

To run the application, please make sure you have all the dependencies installed from the prerequisite list

### Prerequisites

 * NodeJS
 * FAStory Simulator running locally or connection to the actual FAStory line private network.
 
### Running Locally

* Download and install the latest NodeJS. If NodeJS is already installed, 
continue from step 2. 
* Clone the repo in any local directory 
* Run command prompt(cmd) or terminal from that directory 
* In cmd, type 
  `npm install` 
and press enter to install all the dependencies 
* After all dependencies have been installed, run the FASTory simulator. (If you do not have the FASTory line simulator, you may contact FASTlab to acquire a copy for educational purpose)

* Browse to the workstations directory and node start each of the workstation files like 
  ```node wc1.js``` , ```node wc2.js``` ans so on until wc12.js. If you are using IntelliJ IDEA, it is easy to node start twelve separate servers in the same place. If you are using cmd, you may need to start twelve different cmd. One easier alternative is writing one js file that spawns all those twelve workstations as a detached process.  

* Once all the workstation servers and simulation is running, open index.html in any browser and do wonders.

 
 **_You may watch the video (.mp4 file) to have a better clarity about running it locally._**
 
### Running on actual FASTory line

To run it in the actual FASTory line a few changes must be done in order for it to work.

* Firstly you have to ask FASTlab authorities for access to the FASTlab.
* After acquiring access right to the FASTory line legally, the FASTory line must be turned on and you must connect to the private network of the FASTory line. Please ask FAST-LAB authorities for documentation containing the necessary instructions.
* Change the workstation IP addressed in each of the workstation files (wc1.js, wc2,js and so on)
   
 
 
## Author

* **[Abhijit Das](https://www.linkedin.com/in/abhijit-das-jd/)**


## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details


## Acknowledgments

* Special thanks to Lecturer [Andrei Lobov](mailto:andrei.lobov@tut.fi) for his excellent lectures and guidance during the course Distributed Automation Systems Design 
* Gratitude to all my teachers, mentors, book writers and online article writers who provided me with the knowledge required for the project. 
